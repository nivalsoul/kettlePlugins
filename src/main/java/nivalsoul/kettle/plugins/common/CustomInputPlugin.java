package nivalsoul.kettle.plugins.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import cn.hutool.core.io.FileUtil;
import com.alibaba.fastjson.JSON;
import nivalsoul.kettle.plugins.util.excel.HutoolExcelUtil;
import org.pentaho.di.core.exception.KettleStepException;
import org.pentaho.di.core.row.RowDataUtil;
import org.pentaho.di.core.row.RowMeta;
import org.pentaho.di.core.row.ValueMeta;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import nivalsoul.kettle.plugins.util.HttpTool;
import nivalsoul.kettle.plugins.util.excel.XLS2CSVmra;
import nivalsoul.kettle.plugins.util.excel.XLSX2CSV;

public class CustomInputPlugin extends CommonStepRunBase {

	@Override
	public boolean run() throws Exception {
		Object[] r = commonStep.getRow(); 
        if (commonStep.first) {
        	data.outputRowMeta = new RowMeta();
        	meta.getFields(data.outputRowMeta, commonStep.getStepname(), null, null, 
        			commonStep, commonStep.getRepository(), commonStep.getMetaStore());
            commonStep.first = false;
            init();
        }
        //创建输出记录
        String inputType = configInfo.getString("inputType");
        if("rest".equals(inputType)) {
        	getDataFromRest();
        }else if("excel".equals(inputType)) {
        	readFromExcel();
        }else {
        	return doNothing(r);
        }
        
        commonStep.setOutputDone();
        return false;
	}

	private void readFromExcel() throws Exception{
		String filename = commonStep.environmentSubstitute(configInfo.getString("filename"));
		File file = new File(filename);
		if(file.isDirectory()){
			for(File f : FileUtil.loopFiles(file)){
				readExcelFile(f);
			}
		}else{
			readExcelFile(file);
		}
	}

	private void readExcelFile(File f) throws Exception {
		String filename = f.getName();
		commonStep.logBasic("==begin to read file["+filename+"]...");
		int sheetNum = -1;
		if(configInfo.containsKey("sheetNum")){
			sheetNum = Integer.parseInt(commonStep.environmentSubstitute(configInfo.getString("sheetNum")));
		}
		List<List<String>> lists = Lists.newArrayList();
		FileInputStream is = new FileInputStream(f);
		/*if(filename.endsWith(".xls")) {
			lists = XLS2CSVmra.readToList(is);
		} else if(filename.endsWith(".xlsx")) {
			lists = XLSX2CSV.readToList(is);
		} else {
			commonStep.logBasic("file["+filename+"]文件名格式不正确，已跳过...");
		}
		is.close();*/
		if(filename.endsWith(".xls") || filename.endsWith(".xlsx")) {
			lists = new HutoolExcelUtil().readExcel(f, sheetNum);
		}else {
			commonStep.logBasic("file["+filename+"]文件名格式不正确，已跳过...");
		}
		
		if(lists.isEmpty()){
			return;
		}

		int offsetLine = configInfo.getBooleanValue("header") ? 1 : 0;
		//文件数据的字段名
		List<String> fields = Lists.newArrayList();
		//输出的字段信息
		JSONArray outputFields = meta.getOutputFields();
		if(outputFields==null || outputFields.size()==0) {
			outputFields = new JSONArray();
			//使用第一行数据或者自动字段名时，数据类型默认为string
			if(offsetLine == 0) {
				for(int i=0; i<lists.get(0).size();i++) {
					JSONObject field = new JSONObject();
					field.put("name", "field"+(i+1));
					field.put("type", "string");
					outputFields.add(field);
					fields.add("field"+(i+1));
				}
			}else {
				List<String> headerLine = lists.get(0);
				for(int i=0; i<headerLine.size();i++) {
					JSONObject field = new JSONObject();
					field.put("name", headerLine.get(i).trim());
					field.put("type", "string");
					outputFields.add(field);
					fields.add(headerLine.get(i));
				}
			}
			meta.setOutputFields(outputFields);
			meta.getFields(data.outputRowMeta, commonStep.getStepname(), null, null,
					commonStep, commonStep.getRepository(), commonStep.getMetaStore());
		}else{
			if(offsetLine == 1) {
				List<String> headerLine = lists.get(0);
				for(int i=0; i<headerLine.size();i++) {
					String title = headerLine.get(i).trim();
					if(title.equals("")){
						title = "field_"+(i+1);
					}
					fields.add(title);
				}
			}else{
				for(int i=0;i<outputFields.size();i++){
					JSONObject field = outputFields.getJSONObject(i);
					fields.add(field.getString("name"));
				}
			}
		}
		//再次获取字段信息
		outputFields = meta.getOutputFields();

		for(int i=offsetLine; i<lists.size(); i++) {
			List<String> row = lists.get(i);
			//先生成数据行map
			Map<String,String> rowMap = Maps.newHashMap();
			for (int j=0;j<row.size() && j<fields.size();j++) {
				rowMap.put(fields.get(j).trim(), row.get(j));
			}
			//再创建输出数据行，最后一列为文件路径字段
			Object[] outputRowData = RowDataUtil.allocateRowData( data.outputRowMeta.size()+1 );
			for (int j=0;j<outputFields.size();j++) {
				JSONObject field = outputFields.getJSONObject(j);
				String name = field.getString("name");
				String type = field.getString("type");
				if("absFilePath".equals(name)){
					outputRowData[j] = f.getAbsolutePath();
				}else {
					outputRowData[j] = getValueByType(type, rowMap.get(name));
				}
			}
			commonStep.putRow(data.outputRowMeta, outputRowData);
		}
	}

	private void getDataFromRest() throws KettleStepException {
		String url = configInfo.getString("url");
		String method = configInfo.getString("method");
		JSONObject headers = configInfo.getJSONObject("headers");
		if(configInfo.containsKey("useSSL")){
			headers.put("useSSL", configInfo.getBooleanValue("useSSL"));
		}
		String result = null;
		if("get".equalsIgnoreCase(method)) {
			result = HttpTool.get(url, headers);
		}else if("post".equalsIgnoreCase(method)) {
			boolean isUploadFile = configInfo.getBoolean("isUploadFile");
			if(isUploadFile){
				JSONArray fa = configInfo.getJSONArray("files");
				File[] files = new File[fa.size()];
				for (int i = 0; i < fa.size(); i++) {
					files[i] = new File(fa.getString(i));
					result = HttpTool.upload(url, headers, files);
				}
			}else {
				JSONObject params = configInfo.getJSONObject("params");
				Map<String, String> map = Maps.newHashMap();
				for (String key : params.keySet()) {
					map.put(key, params.getString(key));
				}
				String contentType = headers.getString("Content-Type");
				if(contentType!=null && contentType.contains("json")){
					result = HttpTool.post(url, headers, params.toJSONString());
				}else {
					result = HttpTool.post(url, headers, map);
				}
			}
		}
		String resultField = configInfo.getString("resultField");
		addField(data.outputRowMeta, resultField, ValueMeta.TYPE_STRING, 
				ValueMeta.TRIM_TYPE_NONE, commonStep.getStepname(), "结果字段");
		Object[] outputRowData = RowDataUtil.allocateRowData( data.outputRowMeta.size() );
		outputRowData[getFieldIndex(resultField)] = result;
		commonStep.putRow(data.outputRowMeta, outputRowData);
	}
	
	private boolean doNothing(Object[] r) throws Exception, KettleStepException {
		if (r == null) {
		    end();
		    commonStep.setOutputDone();
		    return false;
		}
		commonStep.putRow(data.outputRowMeta, r);
		return true;
	}
	
}
