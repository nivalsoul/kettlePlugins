package nivalsoul.kettle.plugins.test;

import nivalsoul.kettle.plugins.util.bigdata.HdfsUtil;
import org.apache.commons.io.FileUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import java.io.File;
import java.io.IOException;
import java.sql.*;
import java.util.Date;

public class TestHadoop {
    public static void main(String[] args) throws Exception {
        System.setProperty("HADOOP_USER_NAME", "hadoop");

        //HdfsUtil hdfsUtil = new HdfsUtil("192.168.0.123:9000;192.168.0.123:9000");
        //hdfsUtil.uploadToHdfs("F:\\aaaaaa.txt", "/tmp/aaaaaa.txt");

        String localFile = "F:\\aaaaaa.txt";
        String hdfsFile = "/tmp/kkk.txt";
        FileSystem fs = FileSystem.get(getConf("192.168.0.123:9000"));
        long start = new Date().getTime();

       /* InputStream in = new FileInputStream(localFile);
        InputStreamReader isr = new InputStreamReader(in, "UTF-8");
        OutputStream out = fs.create(new Path(hdfsFile), true);
        IOUtils.copy(isr, out, "UTF8");*/

        //该方法更快
        FSDataOutputStream outputStream=fs.create(new Path(hdfsFile));
        String fileContent = FileUtils.readFileToString(new File(localFile), "UTF-8");
        outputStream.write(fileContent.getBytes());
        outputStream.close();
        fs.close();

        //queryTest();
    }

    private static Configuration getConf(String hdfsUrls) {
        String[] urls = hdfsUrls.split(";");
        String ClusterName = "nss";
        String HADOOP_URL = "hdfs://"+ClusterName;
        Configuration conf = new Configuration();
        conf.set("fs.defaultFS", HADOOP_URL);
        conf.set("dfs.nameservices", ClusterName);
        String namenodes = "";
        for (int i=0; i<urls.length; i++) {
            namenodes += ",nn"+i;
            conf.set("dfs.namenode.rpc-address."+ClusterName+".nn"+i, urls[i]);
        }
        conf.set("dfs.ha.namenodes."+ClusterName, namenodes.substring(1));
        conf.set("dfs.client.failover.proxy.provider."+ClusterName,
                "org.apache.hadoop.hdfs.server.namenode.ha.ConfiguredFailoverProxyProvider");
        conf.set("fs.hdfs.impl", "org.apache.hadoop.hdfs.DistributedFileSystem");
        return conf;
    }

    private static void queryTest() throws ClassNotFoundException, SQLException {
        Class.forName("org.apache.hive.jdbc.HiveDriver");
        String url = "jdbc:hive2://192.168.0.123:10000/default";
        Connection hiveCon = DriverManager.getConnection(url, "hadoop", "hadoop");
        Statement stmt = hiveCon.createStatement();

        //stmt.execute("load data inpath '/tmp/aaaaaa.txt' into table t2");

        // 执行SQL语句，得到结果集
        String sql = "select * from t2";
        ResultSet rs = stmt.executeQuery(sql);
        while (rs.next()) {
            System.out.println( rs.getString("data"));
        }
        // 关闭资源
        rs.close();
        stmt.close();
        hiveCon.close();
    }
}
