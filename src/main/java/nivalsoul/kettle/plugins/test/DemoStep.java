package nivalsoul.kettle.plugins.test;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import nivalsoul.kettle.plugins.step.CommonStepData;
import nivalsoul.kettle.plugins.step.CommonStepMeta;
import org.pentaho.di.core.exception.KettleException;
import org.pentaho.di.core.row.RowDataUtil;
import org.pentaho.di.core.row.RowMetaInterface;
import org.pentaho.di.core.row.ValueMeta;
import org.pentaho.di.core.row.ValueMetaInterface;
import org.pentaho.di.trans.Trans;
import org.pentaho.di.trans.TransMeta;
import org.pentaho.di.trans.step.BaseStep;
import org.pentaho.di.trans.step.StepDataInterface;
import org.pentaho.di.trans.step.StepInterface;
import org.pentaho.di.trans.step.StepMeta;
import org.pentaho.di.trans.step.StepMetaInterface;
import org.pentaho.di.trans.steps.userdefinedjavaclass.TransformClassBase;

import java.util.List;

/**
 * @Author 空山苦水禅人
 * @Date 2021/7/31 14:21
 */
public class DemoStep extends BaseStep implements StepInterface {

    private CommonStepData data;
    private CommonStepMeta meta;

    public DemoStep(StepMeta stepMeta, StepDataInterface stepDataInterface, int copyNr, TransMeta transMeta, Trans trans) {
        super(stepMeta, stepDataInterface, copyNr, transMeta, trans);
    }

    @Override
    public boolean processRow(StepMetaInterface smi, StepDataInterface sdi) throws KettleException {
        meta = (CommonStepMeta) smi;
        data = (CommonStepData) sdi;

        Object[] r = getRow();
        //If the row object id null,we are done processing
        //
        if (r == null) {
            setOutputDone();
            return false;
        }

        //Let's look up parameters only once for performance reason.
        //
        if (first) {
            first=false;
        }

        String fields = "#文件开始#软件厂家标志#客货本补#车次#车站号#副司机号#总重#辆数#计长#载重#车速等级#车号#车型#LKJ装置号#车速等级2#车次2#客货本补2#客货本补3#载重2#总重2#辆数2#计长2#车速等级3#副司机号2#车站号2";
        String values = "@@@2021-02-09@@@株洲所@@@货本@@@55002@@@32柳村南@@@2620731王德宝@@@4206@@@210@@@235.0@@@0@@@null@@@271A节@@@HXD1@@@7165@@@null@@@货本55002@@@货本@@@货本@@@0@@@4206@@@210@@@235.0@@@null@@@2620731王德宝@@@32柳村南";
        logBasic("fields=" + fields);
        logBasic("values=" + values);

        try {
            
            String[] fa = fields.substring(1).split("#");
            String[] va = values.substring(3).split("@@@");

            Object[] outputRow = RowDataUtil.allocateRowData( fa.length );
            data.outputRowMeta.clear();
            for (int i = 0; i < fa.length; i++) {
                //add field
                ValueMetaInterface v = new ValueMeta();
                v.setName(fa[i]);
                v.setType(ValueMeta.TYPE_STRING);
                v.setTrimType(ValueMeta.TRIM_TYPE_NONE);
                v.setOrigin(getStepname());
                v.setComments(fa[i]);
                data.outputRowMeta.addValueMeta(v);
                //set value
                outputRow[i] = va[i];
            }
            //putRow will send the row on to the default output hop.
            putRow(data.outputRowMeta, outputRow);

        } catch (Exception e) {
            System.out.println(e.toString());
        }
        
        return true;

    }
}
